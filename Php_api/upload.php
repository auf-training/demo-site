<?php
header("Access-control-Allow-Origin:*");
header("Access-control-Allow-Credentials:true");
header("Access-control-Allow-Methods:POST,GET,DELETE,OPTIONS");
header("Access-control-Allow-Headers:content-type,Authorization,X-Requested-width");
header("content-Type: application/json; charset=UTF-8");
include('server.php');

if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_FILES['file'])) {
    $file = $_FILES['file'];

    // Specify the upload directory
    $uploadDir = 'uploads/';

    // Generate a unique filename
    $filename = uniqid() . '_' . basename($file['name']);
    $uploadPath = $uploadDir . $filename;

    // Move the uploaded file to the specified directory
    if (move_uploaded_file($file['tmp_name'], $uploadPath)) {
        // File uploaded successfully
        echo json_encode(['success' => true, 'filename' => $filename]);
    } else {
        // Error uploading file
        echo json_encode(['success' => false, 'message' => 'Error uploading file.']);
    }
} else {
    // Invalid request
    echo json_encode(['success' => false, 'message' => 'Invalid request.']);
}
?>
 