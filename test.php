<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PHP in HTML</title>
</head>
<body>

    <h1><?php echo "Welcome to my website!"; ?></h1>

    <?php
    $user = "John";
    echo "<p>Hello, $user!</p>";
    ?>

</body>
</html>
