window.onload = function() {
    getListPaiement();
};
function getListPaiement(){
            fetch('Php_api/paiment/listPaiments.php')
            .then(response => response.json()) // Parse the JSON response
            .then(data => displayData(data)) // Display the data

            function displayData(data) {
                var dataList = document.getElementById('container');
            
                // Build the HTML string for the table
                var htmlString = `
                    <table border="1">
                        <thead>
                            <tr>
                                <th>Nom</th>
                                <th>Nombre de Mois</th>
                                <th>Adresse</th>
                                <th>Numéro</th>
                                <th>Type de Paiement</th>
                                <th>Montant</th>
                            </tr>
                        </thead>
                        <tbody>
                            ${data.map(item => `
                                <tr>
                                    <td>${item.nom}</td>
                                    <td>${item.nombreDeMois}</td>
                                    <td>${item.addresse}</td>
                                    <td>${item.numero}</td>
                                    <td>${item.typePaiement}</td>
                                    <td>${item.montant} </td>
                                </tr>
                            `).join('')}
                        </tbody>
                    </table>
                `;
            
                // Set the innerHTML of the container to the HTML string
                dataList.innerHTML = htmlString;
            }

}