
    function uploadFile() {
        var fileInput = document.getElementById('fileInput');
        var file = fileInput.files[0];
    
        var formData = new FormData();
        formData.append('file', file);
        
        fetch('Php_api/upload.php', {
            method: 'POST',
            body: formData
        })
        .then(response => response.json())
        .then(data => {
            document.getElementById('response').innerHTML = 'File uploaded successfully.';
        })
        .catch(error => {
            console.error('Error:', error);
            document.getElementById('response').innerHTML = 'Error uploading file.';
        });
    }