
function submitForm() {
     //first upload file
    var fileInput = document.getElementById('fileInput');
        var file = fileInput.files[0];
    
        var formData = new FormData();
        formData.append('file', file);
        
        fetch('Php_api/upload.php', {
            method: 'POST',
            body: formData
        })
        .then(response => response.json())
        .then(fileSaved => {

            // Data to be sent in the POST request
            const data = {
                firstName: document.getElementById('firstname').value,
                name: document.getElementById('name').value,
                email: document.getElementById('email').value,
                password: document.getElementById('password').value,
                phone: document.getElementById('phone').value,
                imagePath: fileSaved.filename,
            };

            fetch('Php_api/Auth/signUp.php', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(data)
            })

            .then(response => response.json())
            .then(result => {
                console.log('result',result)
                // Access individual elements
                var message = result.message;
                document.getElementById('response').innerText =  'Message: ' + message;
                // window.location.href = './index.html';
            })
            .catch(error => console.error('Error:', error));    
           
        })
        .catch(error => {
            console.error('Error:', error);
            
        });



}


// Attach an event listener to the form's submit event
document.getElementById('myForm').addEventListener('submit', function(event) {
    // Prevent the default form submission behavior
    event.preventDefault();
});




