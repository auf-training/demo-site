
function payForm() {
           const data = {
               numero: document.getElementById('numero').value,
               nom: document.getElementById('nom').value,
               montant: document.getElementById('montant').value,
               typePaiement: document.getElementById('typePaiement').value,
               addresse: document.getElementById('addresse').value,
               nombreDeMois: document.getElementById('nombreDeMois').value,
           };

           fetch('Php_api/paiment/ajout.php', {
               method: 'POST',
               headers: {
                   'Content-Type': 'application/json'
               },
               body: JSON.stringify(data)
           })

           .then(response => response.json())
           .then(result => {
               console.log('result',result)
               var message = result.message;
               document.getElementById('response').innerText =  'Message: ' + message;
                window.location.href = './listPaiement.html';
           })
           .catch(error => console.error('Error:', error));    
}

// Attach an event listener to the form's submit event
document.getElementById('myPayForm').addEventListener('submit', function(event) {
   // Prevent the default form submission behavior
   event.preventDefault();
});




